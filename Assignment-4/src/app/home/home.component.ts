import { Component, OnInit,Inject } from '@angular/core';
import {Dish} from '../shared/dish';
import {Promotion} from '../shared/promotion';
import {Leader} from '../shared/leader';
import {DishService} from '../services/dish.service';
import {PromotionService} from '../services/promotion.service';
import {LeaderService} from '../services/leader.service';
import { flyInOut,expand } from '../animations/app.animation';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  host: {
  '[@flyInOut]': 'true',
  'style': 'display: block;'
  },
  animations: [
    flyInOut(),expand()
  ]
})
export class HomeComponent implements OnInit {

  dish:Dish;
  promotion:Promotion;
  leader:Leader;
  featDishErrMess:string;
  featPromotionErrMess:string;
  featLeaderErrMess:string;


  constructor(private dishservice : DishService,private promotionservice : PromotionService ,private leaderservice : LeaderService,@Inject('BaseURL') private BaseURL) { }

  ngOnInit() {
  this.dishservice.getFeaturedDish().subscribe(dish=>{this.dish=dish},errmess=>{this.featDishErrMess = <any>errmess;console.log(errmess)});
  this.leaderservice.getFeaturedLeader().subscribe(leader=>{this.leader=leader},errmess=>{this.featLeaderErrMess = <any>errmess;console.log(errmess)});
  this.promotionservice.getFeaturedPromotion().subscribe(promotion=>{this.promotion=promotion},errmess=>{this.featPromotionErrMess = <any>errmess;console.log(errmess)});
  }

}
