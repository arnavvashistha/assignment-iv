import { Injectable } from '@angular/core';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';
import { RestangularModule, Restangular } from 'ngx-restangular';
import {Feedback,ContactType} from "../shared/feedback";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FeedbackService {

  constructor(private restangular: Restangular, private processHTTPMsgService: ProcessHttpmsgService) { }

  submitFeedback(feedback:Feedback): Observable<Feedback> {
      let feedbacks = this.restangular.all('/feedback');
      let postInfo = feedbacks.post(feedback);
      return postInfo;
  }

}
