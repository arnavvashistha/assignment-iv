import { Injectable } from '@angular/core';
import {Leader} from '../shared/leader';
import {LEADERS} from '../shared/leaders';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';
import { RestangularModule, Restangular } from 'ngx-restangular';

@Injectable()
export class LeaderService {

  constructor(private restangular: Restangular, private processHTTPMsgService: ProcessHttpmsgService) { }
  getLeaders():Observable<Leader[]>{
    return this.restangular.all('leaders').getList();
  }
  getFeaturedLeader():Observable<Leader>{
    return this.restangular.all('leaders').getList({featured: true})
      .map(leader => leader[0]);
  }
}
